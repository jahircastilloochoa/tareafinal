package mx.lania.ejemplo.tarea.control;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import mx.lania.ejemplo.tarea.entidades.Usuario;
import mx.lania.ejemplo.tarea.oad.OadUsuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class ControladorCatalogos {

    @Autowired
    OadUsuarios oadUsuarios;

   

    @GetMapping("/usuarios")
    public List<Usuario> getUsuarios() {
        return oadUsuarios.findAll();
    }

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<Usuario> getUsuarioPorId(@PathVariable("id") Integer id) {
        Optional<Usuario> opUsuario = oadUsuarios.findById(id);
        if (opUsuario.isPresent()) {
            return ResponseEntity.ok(opUsuario.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/usuarios", params = {"nombre"})
    public List<Usuario> getUsuariosPorNombre(@RequestParam("nombre") String nombre) {
        return oadUsuarios.findByNombreContainingIgnoreCase(nombre);
    }

    @PostMapping("/usuarios")
    public ResponseEntity crearUsuario(@RequestBody @Valid Usuario usuario, Errors errores) {
        try {
            if (errores.hasFieldErrors()) {
                String mensaje = errores.getFieldErrors()
                        .stream()
                        .map(fe -> fe.getField() + " " + fe.getDefaultMessage())
                        .collect(Collectors.joining(","));
                return ResponseEntity
                        .badRequest()
                        .header("ERROR", mensaje)
                        .build();
            }
            oadUsuarios.save(usuario);

            URI urlNuevoUsuario = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .build(usuario.getId());
            return ResponseEntity
                    .created(urlNuevoUsuario)
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity actualizarUsuario(@PathVariable("id") Integer id, @RequestBody Usuario usuario) {
        try {
            usuario.setId(id);
            oadUsuarios.save(usuario);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }

    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity borrarUsuario(@PathVariable("id") Integer id) {
        try {
            oadUsuarios.deleteById(id);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
}
