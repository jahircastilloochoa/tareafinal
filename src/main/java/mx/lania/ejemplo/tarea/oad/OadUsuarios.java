/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.ejemplo.tarea.oad;

import java.util.List;
import mx.lania.ejemplo.tarea.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jahircastilloochoa
 */
public interface OadUsuarios extends JpaRepository<Usuario, Integer>{
    public List<Usuario> findByNombreContainingIgnoreCase(String nombre);    
}
