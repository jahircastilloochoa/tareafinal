package mx.lania.ejemplo.tarea.control;

import mx.lania.ejemplo.tarea.control.ControladorCatalogos;
import java.util.ArrayList;
import java.util.List;
import mx.lania.ejemplo.tarea.entidades.Usuario;
import mx.lania.ejemplo.tarea.oad.OadUsuarios;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControladorCatalogosTest {
    

    @Mock
    OadUsuarios oadUsuarios;
    
    @Autowired
    @InjectMocks
    ControladorCatalogos ctrlCatalogos;
    
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testGetUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add(new Usuario(99));
        when(oadUsuarios.findAll()).thenReturn(usuarios);
        List<Usuario> resultado = ctrlCatalogos.getUsuarios();
        assertThat(resultado).isNotEmpty();
        assertThat(resultado.get(0)).hasFieldOrPropertyWithValue("id", 99);
    }
}
