package mx.lania.ejemplo.tarea.control;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControladorCatalogosIntTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ControladorCatalogosIntTest.class);
    
    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    private String puerto;
    
    
    private static String urlBase;
    
    @Before
    public void definirUrlBase() {
        this.urlBase = "http://localhost:" + puerto;
    }
        
    @Test
    public void testGetUsuarioNoExistente() {
        LOGGER.debug("testGetUsuarioNoExistente");
        ResponseEntity resultado = restTemplate.exchange(this.urlBase + "/usuarios/111", HttpMethod.GET, null, String.class);
        Assertions.assertThat(resultado.getStatusCodeValue())
                .isEqualTo(404);
    }
    
    @Test
    public void testGetUsuarioExistente() {
        LOGGER.debug("testGetUsuarioExistente");
        String resultado = restTemplate.getForObject(this.urlBase + "/usuarios/1", String.class);
        Assertions.assertThat(resultado)
                .startsWith("{")
                .endsWith("}")
                .contains("Jahir");
    }
    
     @Test
    public void testGetUsuarios() {
        LOGGER.debug("testGetUsuarios");
        String resultado = restTemplate.getForObject(this.urlBase + "/usuarios", String.class);
        Assertions.assertThat(resultado)
                .startsWith("[{")
                .endsWith("}]")
                .contains("Jahir");
    }

}
